       IDENTIFICATION DIVISION.
       PROGRAM-ID. MYGRADE.
       AUTHOR. NITHIPHAT.

       ENVIRONMENT DIVISION.
       INPUT-OUTPUT SECTION.
       FILE-CONTROL.
           SELECT SCORE-FILE ASSIGN TO "mygrade.txt"
              ORGANIZATION IS LINE SEQUENTIAL.
           SELECT AVG-FILE ASSIGN TO "avg.txt "
              ORGANIZATION IS LINE SEQUENTIAL.
       DATA DIVISION. 
       FILE SECTION. 
       FD  SCORE-FILE.
       01  SCORE-DETAIL.
       88  END-OF-SCORE-FILE VALUE  HIGH-VALUE. 
           05 SJT-ID PIC X(6).
           05 NAME-SUBJECT PIC  X(50).
           05 CREDITS-SCORE PIC    9.
           05 GRADE-SCORE PIC  X(2).
       FD  AVG-FILE.
       01  AVG-DETAIL.
           05 AVG-GRADES PIC 9v999.
           05 AVG-SCI-GRADES PIC  9V999.
           05 AVG-CS-GRADES PIC  9V999.


       WORKING-STORAGE SECTION.
       01  CRE-GAD-SUM PIC 9V9.
      *SUM GRADE  
       01  CREDITS PIC 999v999.
       01  AVG-GRADE PIC 999v999 .
       01  AVG-SUM-GRADE PIC 9v999.
      *starting with the number 3
       01  STG-DETAIL.
           05 STG-SJT-ID PIC X(6).
           05 STG-NAME-SUBJECT PIC  X(50).
           05 STG-CREDITS-SCORE PIC    9.
           05 STG-GRADE-SCORE PIC  X(2).
      *SCI-GRADE     
       01  STG-SJT-ID-SCI-GRADE REDEFINES STG-DETAIL .
           05 SUBJECT-CODE-SCI PIC A.
      *CS-GRADE     
       01  STG-SJT-ID-CS-GRADE REDEFINES STG-DETAIL .
           05 SUBJECT-CODE-CS PIC AA.   

      *AVG-SCI-GRADE 
       01  CREDITS-SCI PIC 999.
       01  AVG-GRADE-SCI PIC 999v999 .
       01  AVG-SUM-GRADE-SCI PIC 9v999.

      *AVG-CS-GRADE 
       01  CREDITS-CS PIC 999.
       01  AVG-GRADE-CS PIC 999v999 .
       01  AVG-SUM-GRADE-CS PIC 9v999.

       PROCEDURE DIVISION.
       BEGIN.
           OPEN  INPUT SCORE-FILE 

           PERFORM UNTIL END-OF-SCORE-FILE
              READ SCORE-FILE 
                 AT END SET END-OF-SCORE-FILE TO TRUE
              END-READ
              IF NOT END-OF-SCORE-FILE THEN
      *    อ่านข้อมูลในไฟล์ mygrade.txt   
      *          DISPLAY "======================================="
      *          DISPLAY  "SJT: " SJT-ID 
      *          DISPLAY "NAME: " NAME-SUBJECT  " " 
      *          DISPLAY "CREDITS: " CREDITS-SCORE  " "
      *          DISPLAY "GENDER: " GRADE-SCORE 
      *          DISPLAY SCORE-DETAIL
                 PERFORM  001-PROCESS THRU  001-EXIT 
                 COMPUTE CREDITS = CREDITS + CREDITS-SCORE 
                 DISPLAY "ALL CREDITS" " " CREDITS
      *AVG-GRADE            
                 COMPUTE AVG-GRADE = AVG-GRADE + (CREDITS-SCORE * 
                 CRE-GAD-SUM)
                 COMPUTE  AVG-SUM-GRADE = AVG-GRADE / CREDITS
                 DISPLAY "SUM AVG =" " " AVG-SUM-GRADE
      *AVG-GRADE-SCI 
                 IF SUBJECT-CODE-SCI IS EQUAL TO  "3" THEN
                 COMPUTE  CREDITS-SCI  = CREDITS-SCI  + CREDITS-SCORE 
                 COMPUTE  AVG-GRADE-SCI = AVG-GRADE-SCI + (CREDITS-SCORE  
                 * CRE-GAD-SUM ) 
                 COMPUTE  AVG-SUM-GRADE-SCI = AVG-GRADE-SCI /
                 CREDITS-SCI 
                 DISPLAY "SUM AVG SCI =" " " AVG-SUM-GRADE-SCI 
                 END-IF  
      *AVG-CS-GRADE
                 IF SUBJECT-CODE-CS  IS EQUAL TO  "31" THEN
                 COMPUTE  CREDITS-CS   = CREDITS-CS   + CREDITS-SCORE 
                 COMPUTE  AVG-GRADE-CS  = AVG-GRADE-CS  + (CREDITS-SCORE  
                 * CRE-GAD-SUM ) 
                 COMPUTE  AVG-SUM-GRADE-CS  = AVG-GRADE-CS  /
                 CREDITS-CS  
                 DISPLAY "SUM AVG CS =" " " AVG-SUM-GRADE-CS  
                 END-IF   

              END-IF    
           END-PERFORM 
           CLOSE  SCORE-FILE
           OPEN  OUTPUT AVG-FILE
           MOVE  AVG-SUM-GRADE TO AVG-GRADES 
           MOVE  AVG-SUM-GRADE-SCI TO AVG-SCI-GRADES
           MOVE  AVG-SUM-GRADE-CS  TO AVG-CS-GRADES
           WRITE AVG-DETAIL 
           CLOSE  AVG-FILE
           GOBACK 
           .

       001-PROCESS.
           MOVE  SJT-ID  TO STG-SJT-ID 
           EVALUATE  TRUE
              WHEN  GRADE-SCORE IS EQUAL TO "A " MOVE  4  TO CRE-GAD-SUM
              WHEN  GRADE-SCORE IS EQUAL TO "B+" MOVE 3.5 TO CRE-GAD-SUM 
              WHEN  GRADE-SCORE IS EQUAL TO "B " MOVE  3  TO CRE-GAD-SUM 
              WHEN  GRADE-SCORE IS EQUAL TO "C+" MOVE 2.5 TO CRE-GAD-SUM
              WHEN  GRADE-SCORE IS EQUAL TO "C " MOVE  2  TO CRE-GAD-SUM 
              WHEN  GRADE-SCORE IS EQUAL TO "D+" MOVE 1.5 TO CRE-GAD-SUM
              WHEN  GRADE-SCORE IS EQUAL TO "D " MOVE  1  TO CRE-GAD-SUM  
              WHEN  GRADE-SCORE IS EQUAL TO "F " MOVE  0  TO CRE-GAD-SUM 
 
           END-EVALUATE

           .     
       001-EXIT.
           EXIT. 



